<?php
// Solution lab17 INF2005 @ UQAM 
require 'head.php';
require 'config.php';

//-----------
class Participants {
    const ECHEC = 0;
    const SUCCES = 1;
    const PAS_DE_FORMULAIRE = 2;
    private $dossierImages = '.';
    
    protected function enregistreImage(){
        //enregistre une image et retourne le chemin
        if(!isset($_FILES["fichier"])){
            return self::ECHEC;
        }
        $infoFich = $_FILES["fichier"];
        $nomFichier = $infoFich['name']; // nom du fichier chargé par l,utilisateur
        $tmpFile=$infoFich['tmp_name'];// fichier temporaire après téléversement
        $fichierEnregistre=$this->dossierImages."/$nomFichier";
        if(!file_exists($this->dossierImages)){
            mkdir("".$this->dossierImages."")||die("<span style='color:red'>impossible de créer le dossier images: ".$this->dossierImages."</span>");
        }
        move_uploaded_file("$tmpFile","".$fichierEnregistre."")||die("<span style='color:red'>impossible de sauvegarder $nomFichier  dans ".$this->dossierImages."</span>");
        return ($fichierEnregistre);
    }
    
    public function enregistrerForm() {
        global  $database;
        $infoForm = array();
        if(!isset($_REQUEST["soumettre"])){
            return self::PAS_DE_FORMULAIRE;
        }
    
        $infoForm["nom"] = $_REQUEST["nom"];
        $infoForm["prenom"] = $_REQUEST["prenom"];
        $infoForm["genre"] = $_REQUEST["genre"];
        $infoForm["universite"] = $_REQUEST['universite'];
        $infoForm["langues"] = "";
        
        if(!empty($_REQUEST["langues"])){
            $langues = $_REQUEST["langues"];
            $infoForm["langues"] = implode(",",$langues);
            //implode concatene les valeurs du tableau en chaine
            // "," est le séparateur
            // https://www.php.net/manual/en/function.implode.php
        }
        $infoForm["villes"] = "[]";// tebleau des json vide
        if(!empty($_REQUEST['villes'])){ // encodage des villes en json
            $villes = $_REQUEST['villes'];
            $infoForm["villes"] = json_encode($villes,JSON_UNESCAPED_UNICODE|JSON_FORCE_OBJECT);
        }
        $infoForm["photo"] = $this->enregistreImage();
        $infoForm["commentaire"] = $_REQUEST["commentaire"];
        // ici on a deja collecté toutes informations du formulaire
        // on va enregistrer dans la BD
        
        $insertionOk = $database->insert("participants",$infoForm); 
        if($insertionOk){
            return self::SUCCES;
            echo "<h3 class='text-success'>Enregistré avec succès!</h3>";
        }else{
            echo "<h3 class='text-danger'>Echec enregistrement base de données</h3>";
            return self::ECHEC;
        }
        
    }
    
    public function afficherTout() {
        global  $database;
        $enregistrements = $database->select("participants", [
            "nom",
            "prenom",
            "genre",
            "universite",
            "langues",
            "villes",
            "photo",
            "commentaire"
        ]);
        //echo "<pre>";
        //print_r($enregistrements);
        //echo "</pre>";
        echo "<h3>Liste des enregistrements</h2>";
        foreach ($enregistrements as $enregistrement){
            echo "<div class='row'>";
            echo "<div class='col-sm-2'>";//1ere colonne
            
            echo "<img class='img-thumbnail' src='".$enregistrement["photo"]."'alt='photo participant'>";
            echo "</div>";
            echo "<div class='col-sm-4'>"; // 2e colonne
            echo "<span class='h4 text-primary '>Informations</span><br/>";
            echo "<dl class='dl-horizontal'>";
            echo "<dt>Prénom: </dt> <dd>".$enregistrement["prenom"]."</dd>";
            echo "<dt>Nom: </dt> <dd>".$enregistrement["nom"]."</dd>";
            echo "<dt>Genre: </dt> <dd>".$enregistrement["genre"]."</dd>";
            echo "<dt>Université: </dt> <dd>".$enregistrement["universite"]."</dd>";
            echo "<dt>Langues: </dt> <dd>".strtoupper($enregistrement["langues"])."</dd>";
            echo "<dt>Villes: </dt> <dd>";
            $villes = $enregistrement["villes"]; // ATTENTION , un json
            $villes = json_decode($villes,true); // redevient tableau associatif https://www.php.net/manual/fr/function.json-decode.php
            $villes = implode(",",$villes);
            echo "$villes";
            echo "</dd>";
            echo "</dl>";
            
            echo "</div>";
            echo "<div class='col-sm-3'>"; // 3e colonne
            echo "<span class='h4 text-primary'>Commentaire</span><br/>";
            echo $enregistrement["commentaire"];
            echo "</div>";
            echo "</div>";
            
        }
        
    }
    
    function __construct($repImg) {
        
        if(empty($repImg)){
            die("Il faut spécifier le dossier pour enregistrer images");
        }
        $this->dossierImages = $repImg;
    }
}

//---- manipulation classe --
$images = "../HTML/images";// dossier pour enregistrer les images
$part = new Participants($images);
$part->enregistrerForm();
$part->afficherTout();
require 'tail.php';